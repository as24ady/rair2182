package rair2182MV;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.*;

public class BigBangIntegrationT {
    private EmployeeRepositoryInterface employeeRepositoryMock;
    private EmployeeController controller;

    @Before
    public void setUp() {
        try {
            employeeRepositoryMock = new EmployeeMock();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        controller = new EmployeeController(employeeRepositoryMock);
    }

    @Test
    public void unitTestF01(){  // add employee
        try{
            Employee employee = employee = new Employee("Roman", "Adrian", "1961121010203", DidacticFunction.ASISTENT, "3600");
            controller.addEmployee(employee);
        }catch(RepositoryException e){
            assertTrue(false);
        }
    }

    @Test
    public void unitTestF02(){  // update employee
        Employee oldEmployee = new Employee("Roman", "Adrian", "1961124101101", DidacticFunction.ASISTENT, "2400");

        try {
            controller.addEmployee(oldEmployee);
        } catch (RepositoryException e) {
            fail();
        }

        Employee newEmployee = oldEmployee;
        newEmployee.setSalary("3600");
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            assertEquals(controller.getEmployeeByCnp(oldEmployee.getCnp()).getSalary(),"3600");
        }catch(RepositoryException e){
            fail();
        }
    }

    @Test
    public void unitTestF03(){  //getAll
        int size;
        try {
            controller.addEmployee(Employee.getEmployeeFromString("Pacuraru;Marcel;1981202890876;ASISTENT;2000"));
            controller.addEmployee(Employee.getEmployeeFromString("Dumitrescu;Razvan;1961202789087;LECTURER;2500"));
            size = 2;

            List<Employee> employeesList = controller.getEmployeesList();
            assertEquals(employeesList.size(), size);

        }catch (RepositoryException e){
            fail();
        }
        catch (Exception e) {
            System.out.println("eror initializing data");
            e.printStackTrace();
        }
    }

    @Test
    public void BigBang(){
        try {
            controller = new EmployeeController(new EmployeeImpl());

            int initialSize = controller.getEmployeesList().size();

            Employee employee = new Employee("Roman", "Adrian", "1961121010203", DidacticFunction.ASISTENT, "3600");
            controller.addEmployee(employee);//F01

            Employee newEmployee = employee;
            newEmployee.setSalary("4000");
            controller.modifyEmployee(employee, newEmployee);//F02

            List<Employee> employeesList = controller.getEmployeesList();//F03

            assertTrue(employeesList.contains(newEmployee));
            assertEquals(initialSize+1, employeesList.size());

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

}

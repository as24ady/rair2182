package rair2182MV;
import static org.junit.Assert.*;
import salariati.model.Employee;

import org.junit.*;

import salariati.validator.EmployeeValidator;
import salariati.enumeration.DidacticFunction;

public class EmployeeFieldsTest {

	private EmployeeValidator employeeValidator;
	private Employee employee;
	
	@Before
	public void setUp() {
		employeeValidator = new EmployeeValidator();
		employee = new Employee("Roman", "Adrian", "1961121010203", DidacticFunction.ASISTENT, "3600");
	}

	//ECP valid
	@Test
	public void testValidEmployee() {
		assertTrue(employeeValidator.isValid(employee));
	}

	//ECP non-valid
	@Test
	public void testInvalidCNP_letter() {
		employee.setCnp("196a124010203");
		assertFalse(employeeValidator.isValid(employee));
	}
	//ECP non-valid
	@Test
	public void testInvalidSalary_zero() {
		employee.setSalary("0");
		assertFalse(employeeValidator.isValid(employee));
	}
	//ECP non-valid
	@Test
	public void testInvalidSalary_letter() {
		employee.setSalary("asd4");
		assertFalse(employeeValidator.isValid(employee));
	}




	//BVA valid
	@Test
	public void testValidSalary_1() {
		employee.setSalary("1");
		assertTrue(employeeValidator.isValid(employee));
	}
	//BVA valid
	@Test
	public void testValidSalary_2() {
		employee.setSalary("2");
		assertTrue(employeeValidator.isValid(employee));
	}
	//BVA non-valid
	@Test
	public void testInvalidSalary_minus_1() {
		employee.setSalary("-1");
		assertFalse(employeeValidator.isValid(employee));
	}
	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_empty() {
		employee.setCnp("");
		assertFalse(employeeValidator.isValid(employee));
	}

	//-------------------------------------------------------------------
	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_12digits() {
		employee.setCnp("196112401020");
		assertFalse(employeeValidator.isValid(employee));
	}
	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_14digits() {
		employee.setCnp("19611240102030");
		assertFalse(employeeValidator.isValid(employee));
	}



	//FIRSTNAME
	@Test
	public void testOneLetterFirstName(){
		employee.setFirstName("R");
		assertTrue(employeeValidator.isValid(employee));
	}
	@Test
	public  void testInvalidFirstName_digit(){
		employee.setFirstName("A2rian");//ECP invalid nume
		assertFalse(employeeValidator.isValid(employee));
	}
	@Test
	public void testInvalidFirstName_symbol(){
		employee.setFirstName("Invalid#Name");
		assertFalse(employeeValidator.isValid(employee));
	}
	@Test
	public void testInvalidFirstName_empty() {
		employee.setFirstName("");
		assertFalse(employeeValidator.isValid(employee));
	}

	//LASTNAME
	@Test
	public void testOneLetterLastName(){
		employee.setLastName("R");
		assertTrue(employeeValidator.isValid(employee));
	}
	@Test
	public  void testInvalidLastName_digit(){
		employee.setLastName("R2oman");//ECP invalid nume
		assertFalse(employeeValidator.isValid(employee));
	}
	@Test
	public void testInvalidLastName_symbol(){
		employee.setLastName("Invalid#LastName");
		assertFalse(employeeValidator.isValid(employee));
	}
	@Test
	public void testInvalidLastName_empty() {
		employee.setLastName("");
		assertFalse(employeeValidator.isValid(employee));
	}

}

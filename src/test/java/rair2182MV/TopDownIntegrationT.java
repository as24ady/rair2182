package rair2182MV;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

public class TopDownIntegrationT {
    private EmployeeRepositoryInterface employeeRepositoryMock;
    private EmployeeController controller;

    @Before
    public void setUp() {
        try {
            employeeRepositoryMock = new EmployeeMock();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        controller = new EmployeeController(employeeRepositoryMock);
    }

    @Test
    public void unitTestF01(){  // add employee - nonvalid
        Employee employee = new Employee("Roman", "Adrian", "196a124010203", DidacticFunction.ASISTENT, "3600");
        try{
            controller.addEmployee(employee);
        }catch(RepositoryException e){
            assertTrue(true);
        }
    }

    @Test
    public void unitTestF02(){  // update employee
        Employee newEmployee = new Employee("Roman", "Ardrian", "1901124101101", DidacticFunction.ASISTENT, "2400");
        try{
            controller.modifyEmployee(newEmployee, newEmployee);
            fail();
        }catch(RepositoryException e){
            assertEquals("Nu exista angajatul pe care doriti sa il modificati.",e.getMessage());
        }
    }

    @Test
    public void unitTestF03(){  //getAll
        int size;
        try {
            controller.addEmployee(Employee.getEmployeeFromString("Pacuraru;Marcel;1981202890876;ASISTENT;2000"));
            controller.addEmployee(Employee.getEmployeeFromString("Dumitrescu;Razvan;1961202789087;LECTURER;2500"));
            size = 2;

            List<Employee> employeesList = controller.getEmployeesList(3);
            fail(   );

        }catch (RepositoryException e){
            assertTrue(true);
        }
        catch (Exception e) {
            System.out.println("eror initializing data");
            e.printStackTrace();
        }
    }

    @Test
    public void topdownA(){
        try{
            int size = controller.getEmployeesList().size();
            Employee employee = new Employee("Roman", "Adrian", "1961124010203", DidacticFunction.ASISTENT, "3600");
            controller.addEmployee(employee);
            assertEquals(size+1,controller.getEmployeesList().size());
        }catch(RepositoryException e){
            fail();
        }
    }

    @Test
    public void topdownAB(){
        try{
            int size = controller.getEmployeesList().size();
            Employee employee = new Employee("Roman", "Adrian", "1961124010203", DidacticFunction.ASISTENT, "3600");
            controller.addEmployee(employee);
            assertEquals(size+1,controller.getEmployeesList().size());

            Employee newEmployee = employee;
            newEmployee.setFunction(DidacticFunction.TEACHER);
            controller.modifyEmployee(employee, newEmployee);
            assertEquals(controller.getEmployeeByCnp(employee.getCnp()).getFunction().toString(), DidacticFunction.TEACHER.toString());

        }catch(RepositoryException e){
            fail();
        }
    }

    @Test
    public void topdownABC(){
        try{
            int size = controller.getEmployeesList().size();
            Employee employee = new Employee("Roman", "Adrian", "1961124010203", DidacticFunction.ASISTENT, "3600");
            controller.addEmployee(employee);
            assertEquals(size+1,controller.getEmployeesList().size());

            Employee newEmployee = employee;
            newEmployee.setFunction(DidacticFunction.TEACHER);
            controller.modifyEmployee(employee, newEmployee);
            assertEquals(controller.getEmployeeByCnp(employee.getCnp()).getFunction().toString(), DidacticFunction.TEACHER.toString());

            List<Employee> employees = controller.getEmployeesList();
            assertTrue(employees.contains(newEmployee));

        }catch(RepositoryException e){
            fail();
        }
    }
}

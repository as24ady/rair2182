package rair2182MV;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;

import static org.junit.Assert.*;

public class ModifyEmployeeWBT {
    private EmployeeController controller;
    private Employee oldEmployee;

    @Before
    public void setUp() {
        try {
            EmployeeRepositoryInterface employeeRepository = new EmployeeMock();
            oldEmployee = new Employee("Roman", "Adrian", "1961124101101", DidacticFunction.ASISTENT, "2400");
            controller  = new EmployeeController(employeeRepository);

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testModifyEmployee_invalidNewEmployee(){
        try {
            controller.addEmployee(oldEmployee);
        } catch (RepositoryException e) {
            fail();
        }
        Employee newEmployee = new Employee("Roman", "A2sd", "1961124101101", DidacticFunction.ASISTENT, "2400");
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            fail();
        }catch(RepositoryException e){
            assertEquals("Datele noului angajat nu respecta criterile de stocare.",e.getMessage());
        }
    }

    @Test
    public void testModifyEmployee_CNPnotMatch(){
        try {
            controller.addEmployee(oldEmployee);
        } catch (RepositoryException e) {
            fail();
        }
        Employee newEmployee = new Employee("Roman", "Adrian", "1961124101102", DidacticFunction.ASISTENT, "2400");
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            fail();
        }catch(RepositoryException e){
            assertEquals("CNP-ul nu se poate modifica!",e.getMessage());
        }
    }

    @Test
    public void testModifyEmployee_employeesEmpty(){

        Employee newEmployee = oldEmployee;
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            fail();
        }catch(RepositoryException e){
            assertEquals("Nu exista angajatul pe care doriti sa il modificati.",e.getMessage());
        }
    }

    @Test
    public void testModifyEmployee_missingEmployee(){

        Employee newEmployee = oldEmployee;
        oldEmployee.setSalary("1000");
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            fail();
        }catch(RepositoryException e){
            assertEquals("Nu exista angajatul pe care doriti sa il modificati.",e.getMessage());
        }
    }

    @Test
    public void testModifyEmployee_valid(){
        try {
            controller.addEmployee(oldEmployee);
        } catch (RepositoryException e) {
            fail();
        }

        Employee newEmployee = oldEmployee;
        oldEmployee.setSalary("3600");
        try{
            controller.modifyEmployee(oldEmployee, newEmployee);
            assertEquals(controller.getEmployeeByCnp(oldEmployee.getCnp()).getSalary(),"3600");
        }catch(RepositoryException e){
           fail();
        }
    }
}
